# Welcome to this Git Repo!

[[_TOC_]]

## A quick overview of git commands

After making changes to a file and saving them:
```
# Review the changes you made
git status
git diff

# Move stuff from working tree to staging area
git add -p my_file

# Move stuff from staging area to local commit history
git commit
git log

# Check to see what's going on in the remote repo
git fetch
git status
git log

# If necessary, pull in changes from the remote repo,
# and resolve merge conflicts if they arise!
git pull --rebase 

# Sync local commit history with remote repo
git push

```

## Initializing a Git Repo

Set up an SSH key between GitLab and your local machine. Guide is [here][https://docs.gitlab.com/ee/user/ssh.html]

Then you should be able to execute this command from your terminal: `git clone git@gitlab.com:ERM42/git-good.git`

You'll see something like this output:
```
Cloning into 'git-good'...
Enter passphrase for key '/Users/erm/.ssh/id_rsa': 
remote: Enumerating objects: 6, done.
remote: Counting objects: 100% (6/6), done.
remote: Compressing objects: 100% (4/4), done.
remote: Total 6 (delta 0), reused 0 (delta 0), pack-reused 0
Receiving objects: 100% (6/6), done.
```
And now you should have a version of the `git good` repo on your local machine!

## Navigating your Local Git Repo

By far the most frequent git command you'll be using is `git status`. It's the `ls` of git, 
and tells you what all is going on in your git repo. A very handy tool!

If you've modified a tracked file in your local git repo, `git status` will tell you something like this:
```
On branch main
Your branch is up to date with 'origin/main'.

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
        modified:   README.md

no changes added to commit (use "git add" and/or "git commit -a")
```

If you want to view the details of the modifications that have been made to a given file, 
use the `git diff` command.

To move file changes to the staging area, use the `git add` command:
```
# Add all the changes made to the file to the staging area
git add my_file 

# Give you an interactive view and you can pick and choose what changes to add
# (recommended!!)
git add -p my_file  
```

When you do so, `git status` will look like this:
```
On branch main
Your branch is up to date with 'origin/main'.

Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
        modified:   README.md
```

You can then use `git commit` to move whatever is in the staging area to the commit history.
Now, `git status` will tell you this:
```
On branch main
Your branch is ahead of 'origin/main' by 1 commit.
  (use "git push" to publish your local commits)

nothing to commit, working tree clean
```

If you want to see the details of what's in your local commit history 
(and how it compares to the remote commit history), you can use the `git log` command.

## Communicating with the Remote Git Repo

To check for commits and branches made to the remote git repo WITHOUT pulling said changes to your local repo:
```
git fetch
git status
```

To pull down commits and branches from the remote git repo to your local repo:
```
# Use default "merge" routine to resolve differences between remote/local repos
git pull

# Only successfully pulls in changes if things can be "fast forwarded" (recommended!)
git pull --ff-only

# Use "rebase" routine to resolve differences between remote/local repos
# Gives you a nice clean commit history, but can be dangerous when resolving merge conflicts!!!
git pull --rebase
```

To "publish" commits/branches from your local repo to the remote repo:
```
git push
```
If the push is successful, you'll see something like this:
```
Enter passphrase for key '/Users/erm/.ssh/id_rsa': 
Enumerating objects: 5, done.
Counting objects: 100% (5/5), done.
Delta compression using up to 8 threads
Compressing objects: 100% (2/2), done.
Writing objects: 100% (3/3), 1.17 KiB | 599.00 KiB/s, done.
Total 3 (delta 1), reused 0 (delta 0), pack-reused 0
To gitlab.com:ERM42/git-good.git
   863791f..6588d5d  main -> main
```

## Branches and Merging

To make a new branch (starting from local git repo):
```
# this makes a branch that ONLY exists on your local git repo!
git branch my_branch 

# congrats! now your branch exists on the remote git repo also!  
git push origin my_branch  

# this moves you over to your branch
# now any file changes/commits will be put on this branch
git checkout my_branch 
```

To make a new branch (starting from remote git repo):
```
# Use the GitLab web interface to make a new branch for the repo.
# Then execute these commands from your terminal!

# this pulls down the information about the new branch onto your local git repo
git pull --ff-only 

# this moves you over to your branch
# now any file changes/commits will be put on this branch
git checkout my_branch  
```

To merge changes from `master` into `my_branch`:
```
# make sure your local master branch is current with the remote
git checkout master
git pull --ff-only master   

# switch over to my_branch
git checkout my_branch  

# pull in commits from master and apply them to my_branch
git merge master    
```

To merge changes from `my_branch` into `master`:
```
# make sure you're on the master branch!
git checkout master 

# pull in commits from my_branch and apply them to master
git merge my_branch 

# if you're done with a branch, it's good practice to delete it!
git branch -d my_branch 
```

## Some Advanced Notes

### The stash subspace

Sometimes you might want to save some file changes, but not commit them.

You can "hide" them from git by using the stash subspace.
```
# Move your changes to the stash subspace
git stash

# Reapply your stashed changes to your working tree
git stash pop
```

### Hiding stuff from git using `.gitignore`

Are you working on a LaTeX project and wanting to make sure you don't accidentally commit a .pdf file?

Use a `.gitignore` file to tell git to ignore the files you want by adding these lines to the file:
```
# To ignore all .pdf files
*.pdf

# To ignore just one specific .pdf file
manuscript.pdf
```

### Personalizing your git settings with `git config`

Do you hate vim and want to use nano for writing your commit messages?

Do you want to make `git pull --ff-only` your default when you type `git pull`?

Do you want to listen to Shane and make it so your name and email are attached to all your commits?

The `git config` command can help you do all of this! Have fun googling it :)

## Helpful External Resources

The illustrous [Oh Shit, Git!?!][https://ohshitgit.com/] is both helpful and therapeutic!

Atlassian has some [nice tutorials on git][https://www.atlassian.com/git/tutorials]

There's also the [official git website][https://git-scm.com/]

And Parker group people should checkout our own group docummentation pages (yes, page*S*) on git!